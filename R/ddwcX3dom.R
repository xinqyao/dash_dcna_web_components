# AUTO GENERATED FILE - DO NOT EDIT

ddwcX3dom <- function(id=NULL, width=NULL, height=NULL, url=NULL, sceneCont=NULL) {
    
    props <- list(id=id, width=width, height=height, url=url, sceneCont=sceneCont)
    if (length(props) > 0) {
        props <- props[!vapply(props, is.null, logical(1))]
    }
    component <- list(
        props = props,
        type = 'X3dom',
        namespace = 'dash_dcna_web_components',
        propNames = c('id', 'width', 'height', 'url', 'sceneCont'),
        package = 'dashDcnaWebComponents'
        )

    structure(component, class = c('dash_component', 'list'))
}
