# AUTO GENERATED FILE - DO NOT EDIT

from dash.development.base_component import Component, _explicitize_args


class X3dom(Component):
    """A X3dom component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.

Keyword arguments:
- id (string; default 'x3d'): The ID used to identify this component in Dash callbacks.
- width (string; default '600px'): The width of the display window.
- height (string; default '400px'): The height of the display window.
- url (string; default ''): The path/url to the x3d file to display. Obsolete.
- sceneCont (string; default ''): The content of x3d nodes to display."""
    @_explicitize_args
    def __init__(self, id=Component.UNDEFINED, width=Component.UNDEFINED, height=Component.UNDEFINED, url=Component.UNDEFINED, sceneCont=Component.UNDEFINED, **kwargs):
        self._prop_names = ['id', 'width', 'height', 'url', 'sceneCont']
        self._type = 'X3dom'
        self._namespace = 'dash_dcna_web_components'
        self._valid_wildcard_attributes =            []
        self.available_properties = ['id', 'width', 'height', 'url', 'sceneCont']
        self.available_wildcard_properties =            []

        _explicit_args = kwargs.pop('_explicit_args')
        _locals = locals()
        _locals.update(kwargs)  # For wildcard attrs
        args = {k: _locals[k] for k in _explicit_args if k != 'children'}

        for k in []:
            if k not in args:
                raise TypeError(
                    'Required argument `' + k + '` was not specified.')
        super(X3dom, self).__init__(**args)
