#!/bin/bash

if test ! -r R_build; then
   mkdir R_build
fi

rm -rf R_build/{R,man,inst,DESCRIPTION,NAMESPACE}
cp -r R man DESCRIPTION NAMESPACE R_build/

mkdir -p R_build/inst/deps
cp inst/deps/dash_dcna_web_components.min.js* R_build/inst/deps/

## build it in Rstudio or
#R CMD build R_build
