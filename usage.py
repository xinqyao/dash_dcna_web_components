import dash_dcna_web_components
import dash
from dash.dependencies import Input, Output
import dash_html_components as html

app = dash.Dash(__name__)

f = open("demo.html", "r")  
dat = f.read()

app.layout = html.Div([
	html.P('test!'),
	html.Div(
       dash_dcna_web_components.X3dom(
    	   id='x3d',
           width='600px',
           height='400px',
           sceneCont=dat
    )),
    html.Div(id='output')
])


@app.callback(Output('output', 'children'), [Input('x3d', 'width')])
def display_output(url):
    return 'You have entered width: {}'.format(url)


if __name__ == '__main__':
    app.run_server(debug=True)
