import React, {Component, createRef} from 'react';
import PropTypes from 'prop-types';
//import './x3dom.css';
//import 'x3dom';

/**
 * ExampleComponent is an example component.
 * It takes a property, `label`, and
 * displays it.
 * It renders an input with the property `value`
 * which is editable by the user.
 */

export default class X3dom extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.sceneRef = createRef();
    }

    componentDidMount() {
        this.sceneRef.current.innerHTML = this.props.sceneCont;
        window.x3dom.reload();
    }

    componentDidUpdate() {
        this.sceneRef.current.innerHTML = this.props.sceneCont;
        window.x3dom.reload();
    }
  
    componentWillUnmount() {
    }

    render() {
        const {id, width, height, sceneCont} = this.props;
        return (
                <x3d is='x3d' showStat='false' showLog='false' id={id} width={width} height={height}>
                    <scene is='scene' def='scene' ref={this.sceneRef} />
                </x3d>
        )
    }
}

X3dom.defaultProps = {
    id: 'x3d',
    width: '600px',
    height: '400px',
    url: '',
    sceneCont: ''
}

X3dom.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string.isRequired,

    /**
     * The width of the display window.
     */
    width: PropTypes.string,

    /**
     * The height of the display window.
     */
    height: PropTypes.string,

    /**
     * The path/url to the x3d file to display. Obsolete.
     */
    url: PropTypes.string,

    /**
     * The content of x3d nodes to display.
     */
    sceneCont: PropTypes.string.isRequired
}
