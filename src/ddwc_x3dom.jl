# AUTO GENERATED FILE - DO NOT EDIT

export ddwc_x3dom

"""
    ddwc_x3dom(;kwargs...)

A X3dom component.
ExampleComponent is an example component.
It takes a property, `label`, and
displays it.
It renders an input with the property `value`
which is editable by the user.
Keyword arguments:
- `id` (String; optional): The ID used to identify this component in Dash callbacks.
- `width` (String; optional): The width of the display window.
- `height` (String; optional): The height of the display window.
- `url` (String; optional): The path/url to the x3d file to display. Obsolete.
- `sceneCont` (String; optional): The content of x3d nodes to display.
"""
function ddwc_x3dom(; kwargs...)
        available_props = Symbol[:id, :width, :height, :url, :sceneCont]
        wild_props = Symbol[]
        return Component("ddwc_x3dom", "X3dom", "dash_dcna_web_components", available_props, wild_props; kwargs...)
end

