/* eslint no-magic-numbers: 0 */
import React, {Component} from 'react';

import { X3dom } from '../lib';

class App extends Component {

    constructor() {
        super();
        this.state = {
            value: ''
        };
        this.setProps = this.setProps.bind(this);
    }

    setProps(newProps) {
        this.setState(newProps);
    }

    render() {
        return (
            <div>
                <h1> Hello, World!</h1>
                <X3dom
           id='x3d'
           width='600px'
           height='400px'
           url='demo.x3d'
                />
            </div>
        )
    }
}

export default App;
