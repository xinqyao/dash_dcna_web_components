% Auto-generated: do not edit by hand
\name{ddwcX3dom}

\alias{ddwcX3dom}

\title{X3dom component}

\description{
ExampleComponent is an example component. It takes a property, `label`, and displays it. It renders an input with the property `value` which is editable by the user.
}

\usage{
ddwcX3dom(id=NULL, width=NULL, height=NULL, url=NULL, sceneCont=NULL)
}

\arguments{
\item{id}{Character. The ID used to identify this component in Dash callbacks.}

\item{width}{Character. The width of the display window.}

\item{height}{Character. The height of the display window.}

\item{url}{Character. The path/url to the x3d file to display. Obsolete.}

\item{sceneCont}{Character. The content of x3d nodes to display.}
}

\value{named list of JSON elements corresponding to React.js properties and their values}

